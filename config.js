// get a client secret and id and define the one on the server through this:
// `docker exec -it <name_of_the_pagedjs_server_container> yarn create:client`

// oldest one
// const clientId = "59a3392b-0c4f-4318-bbe2-f86eff6d3de4";
// const clientSecret = "asldkjLKJLaslkdf897kjhKUJH";

const clientId = "59a3392b-0c4f-4318-bbe2-f86eff6d3de4";
const clientSecret = "asldkjLKJLaslkdf897kjhKUJH";

//get the protocol from the docker image
const protocol = "http";
const host = "localhost";
const port = "3000";

//file that will be sent to the docker image
const zipPath = "./test.zip";

module.exports = {
  clientId,
  clientSecret,
  protocol,
  host,
  port,
  zipPath,
};
