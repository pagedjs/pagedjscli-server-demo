// import addIDtoEachElement from "./modules/addid.js"

// Set the handler
class elifeBuild extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller)
  }

  beforeParsed(content) {
    hideFigureBlock(content)
    hidenumber(content)
    addTitleToPeerReview(content)
    addIDtoEachElement(content)
    removescript(content)
    removeFigureList(content)
    spaceToNbsp(content)
    createElForRunningHead(content) // ←
    addDateBeforeDOI(content)
    // moveSummary(content)
    // removeMeta(content)
    addClassToConsecutiveLinks(content)
    movelabelToCaption(content)
    moveTheSecondColumnToTop(content) // ←
    // addCorrespondingAuthor(content)
    // reorderReview(content)
  }
  afterParsed(parsed) {
    imageRatio(parsed)
  }
  afterPageLayout(page) {
    // fixBreak(page)
  }
  afterRendered(pages) {
    // moveBlocPageOne(pages)
  }
}

Paged.registerHandlers(elifeBuild)

function hidenumber(content) {
  content.querySelectorAll('p').forEach((p) => {
    if (!p.innerHTML.match(/[A-z]/g)) {
     p.classList.add("onlynumbers") 
    }
  })
}

function addCorrespondingAuthor(content) {
  if (content.querySelector('.contributors .corresp-list')) {
    content
      .querySelector('.institutions_institutions-list__g1Bu7')
      .insertAdjacentHTML(
        'afterend',
        content.querySelector('.contributors .corresp-list').outerHTML
      )
      content.querySelector('.corresp-list').id = "corresponding"
      content.querySelector('.corresp-list').dataset.id = "corresponding"
      content.querySelector('.corresp-list').classList.add("corresponding")
  }
  if (content.querySelector('.contributors .contributor-list')) {
    const existingAuthorsList = content.querySelector('.authors_authors-list__qIQOP');
    existingAuthorsList
      .insertAdjacentHTML(
        'afterend',
        content.querySelector('.contributors .contributor-list').outerHTML
      )
    // existingAuthorsList.classList.add('hide')
    existingAuthorsList.remove();
    content.querySelector('.contributor-list').classList.add("authors_authors-list__qIQOP")
    content.querySelectorAll('.contributor-list li').forEach(li => li.classList.add("authors_authors-list__item__tb24B"))
  }
  if (content.querySelector('.contributors .affiliation-list')) {
    const existingAffliationsList = content.querySelector('.institutions_institutions-list__g1Bu7');
    existingAffliationsList
      .insertAdjacentHTML(
        'afterend',
        content.querySelector('.contributors .affiliation-list').outerHTML
      )
    // existingAffliationsList.classList.add('hide')
    existingAffliationsList.remove();
    content.querySelector('.affiliation-list').classList.add("institutions_institutions-list__g1Bu7")
    content.querySelectorAll('.affiliation-list li').forEach(li => li.classList.add("institutions_institutions-list__item__0P_F0"))
  }
  // content.querySelector('.contributors').classList.add('hide')
  content.querySelector('.contributors').remove();
}

function addClassToConsecutiveLinks(content) {
  content.querySelectorAll('span a + a').forEach((link) => {
    link.classList.add('nextlink')
  })
}

function hideFigureBlock(content) {
  content.querySelector('#figures').closest('div').style.display = 'none'
}

function reorderReview(content) {
  let reviewsOrdered = document.createElement('section')
  reviewsOrdered.classList.add('review-ordered')
  let wrap = Array.from(
    content
      .querySelector('#editors-and-reviewers')
      .closest('div')
      .querySelectorAll('section')
  )
  console.log(wrap)
  for (let index = 0; index < wrap.length; index++) {
    const element = wrap[index]
    console.log(element.className)
    console.log(element)
    if (element.className.includes('review-content_review-content')) {
      reviewsOrdered.insertAdjacentElement('afterbegin', element)
    }
  }
  console.log(reviewsOrdered)
  content
    .querySelector('#editors-and-reviewers')
    .closest('div')
    .insertAdjacentElement('beforeend', reviewsOrdered)
  // wrap.querySelectorAll('section').forEach( review =>{
  //
  //   console.log(review)
  // })
}

function addTitleToPeerReview(content) {
  content
    .querySelector('#editors-and-reviewers')
    .closest('section')
    .classList.add('peerreviews')
  content
    .querySelector('#editors-and-reviewers')
    .closest('section')
    .insertAdjacentHTML('afterbegin', `<h1>Peer review</h1>`)
}

function boldTermsInAssessment(content) {
  let block = content.querySelector('#evaluation-summary').closest('section')
  const terms = [
    'tour-de-force',
    'compelling',
    'convincing',
    'solid',
    'incomplete',
    'inadequate',
    'landmark',
    'fundamental',
    'important',
    'noteworthy',
    'useful',
    'flawed',
  ]
  terms.forEach((term) => {
    block.innerHTML = block.innerHTML.replace(term, `<strong>${term}</strong>`)
  })
}

function changeReviewSummaryTitle(content) {
  content.querySelector('#evaluation-summary').innerHTML = 'eLife assessment'
}

function moveBlocPageOne(pages) {
  const offset = -40

  const block = document.querySelector('.colBlock')
  let bottom = block
    .closest('.pagedjs_page_content')
    .querySelector('div').lastChild
  var bounding = bottom.getBoundingClientRect()
  var blockBounding = block.getBoundingClientRect()
  console.log(bounding)
  console.log(blockBounding)
  block.style.top = `${bounding.bottom - bounding.top - blockBounding.height}px`
  console.log(bounding)
}

function moveTheSecondColumnToTop(content) {
  const host = content.querySelector('#__next')
  host.insertAdjacentHTML(
    'beforebegin',
    `<section class="colBlock">${
      content.querySelector('aside').innerHTML
    }</section>`
  )
  content.querySelector('aside').style.display = 'none'
}

function movelabelToCaption(content) {
  content.querySelectorAll('figure').forEach((fig) => {
    const label = fig.querySelector('label')
    const caption = fig.querySelector('figcaption')
    if (label && caption) {
      caption.insertAdjacentElement('afterbegin', label)
    }
  })
}

function removeMeta(content) {
  content.querySelectorAll('meta').forEach((el) => el.remove())
}

function addDateBeforeDOI(content) {
  // the date
  //
  let date = ''
  content.querySelectorAll('dd').forEach((dd) => {
    if (
      dd.className.includes('timeline_review-timeline__date') &&
      date === ''
    ) {
      dd.querySelector('a')?.remove()
      return (date = dd.textContent)
    }
  })

  // find the doi link

  content.querySelectorAll('a').forEach((link) => {
    if (link.href.includes('doi.org')) {
      let year = new Date (date);
      link.insertAdjacentHTML(
        'afterbegin',
        `<span class="date">${year.getFullYear()} eLife</span>. `
      )
    }
  })
  // content
  //   .querySelector('.content-header__identifier')?
  //   .insertAdjacentHTML(
  //     'afterbegin',
  //     `<span class="date">${
  //       content.querySelector('.review-timeline__date').innerHTML
  //     }</span>, `
  //   )
}

// get the image ratio to define a first layout

function imageRatio(parsed) {
  let imagePromises = []
  let images = parsed.querySelectorAll('img')
  images.forEach((image) => {
    let img = new Image()
    let resolve, reject
    let imageLoaded = new Promise(function (r, x) {
      resolve = r
      reject = x
    })

    img.onload = function () {
      let height = img.naturalHeight

      let width = img.naturalWidth

      let ratio = width / height
      if (ratio >= 1.7) {
        image.classList.add('landscape')
        image.parentNode.classList.add('fig-landscape')
      } else if (ratio <= 0.95) {
        image.classList.add('portrait')
        image.parentNode.classList.add('fig-portrait')
      } else if (ratio < 1.39 || ratio > 0.95) {
        image.classList.add('square')
        image.parentNode.classList.add('fig-square')
      }

      // all images as square
      // image.classList.add("square");
      // image.parentNode.classList.add("fig-square");

      resolve()
    }
    img.onerror = function () {
      reject()
    }

    img.src = image.src

    imagePromises.push(imageLoaded)
  })
  return Promise.all(imagePromises).catch((err) => {
    console.warn(err)
  })
}

//  TODO: try to fix the multiple reak-after: avoid;
function fixBreak(page) {
  let final = page.lastChild
  if (page.querySelector('[data-break-after="avoid"]')) {
  }
}

// To use to remove hyphens between pages
function getFinalWord(words) {
  var n = words.split(' ')
  return n[n.length - 1]
}

// add nbsp in list
function spaceToNbsp(content) {
  let authorList = content.querySelectorAll(
    '.authors li, .authors_authors-list__qIQOP li'
  )
  // let affiliationList = content.querySelector('.authors').nextElementSibling.querySelectorAll('li')
  console.log(authorList)
  authorList.forEach(
    (person) =>
      (person.innerHTML = person.innerHTML.trim().replace(/\s+/g, '&nbsp;'))
  )
}

// add elife caution
function addElifeTextBeforeAbstract(content) {
  let abstract = content.querySelector('#evaluation-summary').closest('section')
  abstract.insertAdjacentHTML(
    'beforebegin',
    `<section class="elife-caution">${
      content.querySelector('.evaluation-summary').innerHTML
    }</section>`
  )
  content.querySelector('.evaluation-summary').style.display = 'none'
  // querySelector("#abstract")?.closest('section');
}
//move summary
function moveSummary(content) {
  let summaries = content.querySelectorAll('.evaluation-summary')
  content
    .querySelector('#abstract')
    .closest('section')
    .insertAdjacentElement('afterend', summaries[0])
}
// rmeove scripts

function removescript(content) {
  content.querySelectorAll('script').forEach((script) => {
    console.log(script.innerHTML)
    script.innerHTML = ''
    script.remove()
  })
}

// create a table of content

let toctags = 'h2, h3'

function createTOC(content) {
  let nav = document.createElement('nav')
  nav.innerHTML = '<h2>Table of contents</h2>'
  let toclist = document.createElement('ul')
  content.querySelectorAll(toctags).forEach((tag) => {
    toclist.insertAdjacentHTML(
      'beforeend',
      `<li class="toc-${tag.tagName.toLowerCase()}"><a href="#${
        tag.id
      }">${tag.innerHTML.trim()}</a></li>`
    )
  })
  nav.insertAdjacentElement('beforeend', toclist)
  content
    .querySelector('.evaluation-summary')
    .insertAdjacentElement('afterend', nav)
}

/*========================== 
     addIDtoEachElement
========================== */

// Define here the tags you want to give id
let tags = [
  'figure',
  'figcaption',
  'img',
  'ol',
  'ul',
  'li',
  'p',
  'img',
  'table',
  'h1',
  'h2',
  'h3',
  'h4',
  'div',
  'aside',
]

function addIDtoEachElement(content) {
  let total = 0
  tags.forEach((tag) => {
    content.querySelectorAll(tag).forEach((el, index) => {
      if (!el.id) {
        if (el.tagName == 'p') {
          if (el.closest('figcaption')) {
            return
          }
        }

        el.id = `el-${el.tagName.toLowerCase()}-${index}`
        total++
      }
    })
  })
  console.log(`added ${total} ids!`)
}

/*========================== 
     openAllDetailsAndSummary
========================== */

function openAllDetailsAndSummary(content) {
  content.querySelectorAll('details').forEach((detail) => {
    detail.setAttribute('open', 'open')
  })
}

/*====================================== 
     fix citation
====================================== */

function citeFix(content) {
  let bibliography = document.createElement('ul')

  bibliography.classList.add('bibliography')
  bibliography.innerHTML = '<h2>References</h2>'
  content.querySelectorAll('cite').forEach((cite) => {
    bibliography.insertAdjacentHTML(
      'beforeend',
      `<li>${cite.innerHTML.trim()}</li>`
    )
  })
  content.append(bibliography)
}

/*====================================== 
     duplicate and move figures
====================================== */

function moveFigures(content) {
  let figures = document.createElement('section')

  figures.classList.add('figureWrapper')
  figures.innerHTML = '<h2>Figures</h2>'
  content.querySelectorAll('figure').forEach((figure) => {
    figures.insertAdjacentHTML(
      'beforeend',
      `<figure class="movedfigure">${figure.innerHTML.trim()}</figure>`
    )
  })
  content.append(figures)
}

/*====================================== 
     Create the element used for runningHead 
====================================== */

function createElForRunningHead(content) {
  //doi is coming from the document body
  let author = ''
  let authors = content.querySelectorAll(
    '.authors li, .authors_authors-list__qIQOP li'
  )
  for (let i = 0; i < 2; i++) {
    author = `${author}${
      i == 0 ? authors[i].innerHTML : ', ' + authors[i].innerHTML
    }`
  }

  // let elifelink = window.location
  // const doi = elifelink.pathname.substring(0, elifelink.pathname.length - 1);
  const doi = document.querySelector('body').dataset.doi
  content.querySelector('div').insertAdjacentHTML(
    'afterbegin',
    `<p class='runninghead'><span class="author">${author} <em>et al.</em></span>,&nbsp;<span class="doi">
      ${
        doi
          ? `<a href="https://doi.org/${doi}/">https://doi.org/${doi}</a>`
          : ''
      }</span> <span class="counter"></p></p>`
  )
}

function removeFigureList(content) {
  content.querySelector('#figures').closest('div').remove()
}

// get ratio for images and add classes based on taht.

// no hyphens between page
class noHyphenBetweenPage extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller)
    this.hyphenToken
  }

  afterPageLayout(pageFragment, page, breakToken) {
    if (pageFragment.querySelector('.pagedjs_hyphen')) {
      // find the hyphenated word
      let block = pageFragment.querySelector('.pagedjs_hyphen')

      // i dont know what that line was for :thinking: i removed it
      // block.dataset.ref = this.prevHyphen;

      // move the breakToken
      let offsetMove = getFinalWord(block.innerHTML).length

      // move the token accordingly
      page.breakToken = page.endToken.offset - offsetMove

      // remove the last word
      block.innerHTML = block.innerHTML.replace(
        getFinalWord(block.innerHTML),
        ''
      )

      breakToken.offset = page.endToken.offset - offsetMove
    }
  }
}

Paged.registerHandlers(noHyphenBetweenPage)

class pushThings extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller)
    this.pushblock = []
  }
  onDeclaration(declaration, dItem, dList, rule) {
    // move the element to the next bit
    if (declaration.property == '--experimental-push') {
      let sel = csstree.generate(rule.ruleNode.prelude)
      sel = sel.replace('[data-id="', '#')
      sel = sel.replace('"]', '')
      let itemsList = sel.split(',')
      itemsList.forEach((elId) => {
        this.pushblock.push([elId, declaration.value.value])
      })
    }
  }
  afterParsed(parsed) {
    if (this.pushblock.length > 0) {
      this.pushblock.forEach((elToPush) => {
        const elem = parsed.querySelector(elToPush[0])
        if (!elem) {
          return
        }

        elem.dataset.pushBlock = elToPush[1]
        let direction = ''
        if (elToPush[1] < 0) {
          direction = 'back'
        }
        if (direction == 'back') {
          for (let index = 0; index < Math.abs(elToPush[1]); index++) {
            if (elem.previousElementSibling) {
              elem.previousElementSibling.insertAdjacentElement(
                'beforebegin',
                elem
              )
            }
          }
        } else {
          for (let index = 0; index < Math.abs(elToPush[1]); index++) {
            if (elem.nextElementSibling) {
              elem.nextElementSibling.insertAdjacentElement('beforebegin', elem)
            }
          }
        }
      })
    }
  }
}

Paged.registerHandlers(pushThings)

class urlSwitcher extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller)
  }

  beforeParsed(content) {
    const imageUrl = document.body.id
    console.log(imageUrl)
    content.querySelectorAll('img').forEach((img) => {
      img.src =
        '/images/' +
        imageUrl +
        '/' +
        img.src.split('/')[img.src.split('/').length - 1]
    })
    // find a place to put the content, i would say after the index
  }
}

// Paged.registerHandlers(urlSwitcher);

class CSStoClass extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller)
    this.floatSameTop = []
    this.floatSameBottom = []
    this.floatNextTop = []
    this.floatNextBottom = []
    this.experimentalImageEdit = []
    this.spacing = []
    this.pushblock = []
    this.experimentalMerged = []
    this.fullPageBackground = []
  }
  onDeclaration(declaration, dItem, dList, rule) {
    // alter the image
    if (declaration.property == '--experimental-image-edit') {
      let sel = csstree.generate(rule.ruleNode.prelude)
      sel = sel.replace('[data-id="', '#')
      sel = sel.replace('"]', '')
      let itemsList = sel.split(',')
      itemsList.forEach((elId) => {
        this.experimentalImageEdit.push(elId)
      })
    }
    // move the element to the next bit
    else if (declaration.property == '--experimental-push') {
      let sel = csstree.generate(rule.ruleNode.prelude)
      sel = sel.replace('[data-id="', '#')
      sel = sel.replace('"]', '')
      let itemsList = sel.split(',')
      itemsList.forEach((elId) => {
        this.pushblock.push([elId, declaration.value.value])
      })
    } else if (declaration.property == '--experimental-fullpage') {
      let sel = csstree.generate(rule.ruleNode.prelude)
      sel = sel.replace('[data-id="', '#')
      sel = sel.replace('"]', '')
      let itemsList = sel.split(',')
      itemsList.forEach((elId) => {
        this.fullPageBackground.push([elId, declaration.value.value])
      })
    }
    //experimental merge
    else if (declaration.property == '--experimental-merge') {
      let sel = csstree.generate(rule.ruleNode.prelude)
      sel = sel.replace('[data-id="', '#')
      sel = sel.replace('"]', '')
      let itemsList = sel.split(',')
      itemsList.forEach((elId) => {
        this.experimentalMerged.push([elId, declaration.value.value])
      })
    }
    // page floats
    else if (declaration.property == '--experimental-page-float') {
      if (declaration.value.value.includes('same-top')) {
        let sel = csstree.generate(rule.ruleNode.prelude)
        sel = sel.replace('[data-id="', '#')
        sel = sel.replace('"]', '')
        this.floatSameTop.push(sel.split(','))
        console.log('floatSameTop: ', this.floatSameTop)
      } else if (declaration.value.value.includes('same-bottom')) {
        let sel = csstree.generate(rule.ruleNode.prelude)
        sel = sel.replace('[data-id="', '#')
        sel = sel.replace('"]', '')
        this.floatSameBottom.push(sel.split(','))
        //console.log("floatSameBottom: ", this.floatSameBottom);
      } else if (declaration.value.value.includes('next-top')) {
        let sel = csstree.generate(rule.ruleNode.prelude)
        sel = sel.replace('[data-id="', '#')
        sel = sel.replace('"]', '')
        this.floatNextTop.push(sel.split(','))
        //console.log('floatNextTop: ', this.floatNextTop);
      } else if (declaration.value.value.includes('next-bottom')) {
        let sel = csstree.generate(rule.ruleNode.prelude)
        sel = sel.replace('[data-id="', '#')
        sel = sel.replace('"]', '')
        this.floatNextBottom.push(sel.split(','))
        //console.log("floatNextBottom: ", this.floatNextBottom);
      }
    }
    // spacing
    else if (declaration.property == '--experimental-spacing') {
      var spacingValue = declaration.value.value
      spacingValue = spacingValue.replace(/\s/g, '')
      spacingValue = parseInt(spacingValue)
      let sel = csstree.generate(rule.ruleNode.prelude)
      sel = sel.replace('[data-id="', '#')
      sel = sel.replace('"]', '')
      var thisSpacing = [sel.split(','), spacingValue]
      this.spacing.push(thisSpacing)
    }
  }

  afterParsed(parsed) {
    if (this.pushblock.length > 0) {
      this.pushblock.forEach((elToPush) => {
        const elem = parsed.querySelector(elToPush[0])
        if (!elem) {
          return
        }

        elem.dataset.pushBlock = elToPush[1]
        let direction = ''
        if (elToPush[1] < 0) {
          direction = 'back'
        }
        if (direction == 'back') {
          for (let index = 0; index < Math.abs(elToPush[1]); index++) {
            if (elem.previousElementSibling) {
              elem.previousElementSibling.insertAdjacentElement(
                'beforebegin',
                elem
              )
            }
          }
        } else {
          for (let index = 0; index < Math.abs(elToPush[1]); index++) {
            if (elem.nextElementSibling) {
              elem.nextElementSibling.insertAdjacentElement('afterend', elem)
            }
          }
        }
      })
    }
    if (this.experimentalMerged.length > 0) {
      this.experimentalMerged.forEach((couple) => {
        const host = parsed.querySelector(couple[0])
        const guest = parsed.querySelector(couple[1])
        if (!host || !guest) {
          return
        }
        guest.style.display = 'none'
        host.classList.add('merged!')
        host.dataset.mergedGuest = guest.id
        host.insertAdjacentHTML('beforeend', guest.innerHTML)
      })
    }
    if (this.experimentalImageEdit.length > 0) {
      this.experimentalImageEdit.forEach((elNBlist) => {
        parsed.querySelectorAll(elNBlist).forEach((img) => {
          img.classList.add('imageMover')

          // console.log("#" + img.id + ": image Mover");
        })
      })
    }
    if (this.floatNextBottom) {
      this.floatNextBottom.forEach((elNBlist) => {
        parsed.querySelectorAll(elNBlist).forEach((el) => {
          el.classList.add('page-float-next-bottom')
          // console.log("#" + el.id + " moved to next-bottom");
        })
      })
    }
    if (this.floatNextTop) {
      this.floatNextTop.forEach((elNBlist) => {
        parsed.querySelectorAll(elNBlist).forEach((el) => {
          el.classList.add('page-float-next-top')
          // console.log("#" + el.id + " moved to next-top");
        })
      })
    }
    if (this.floatSameTop) {
      this.floatSameTop.forEach((elNBlist) => {
        parsed.querySelectorAll(elNBlist).forEach((el) => {
          el.classList.add('page-float-same-top')
          // console.log("#" + el.id + " moved to same-top");
        })
      })
    }
    if (this.floatSameBottom) {
      this.floatSameBottom.forEach((elNBlist) => {
        parsed.querySelectorAll(elNBlist).forEach((el) => {
          el.classList.add('page-float-same-bottom')
          // console.log("#" + el.id + " moved to same-bottom");
        })
      })
    }
    if (this.spacing) {
      this.spacing.forEach((elNBlist) => {
        parsed.querySelectorAll(elNBlist[0]).forEach((el) => {
          var spacingValue = elNBlist[1]
          var spacingClass = 'spacing-' + spacingValue
          // console.log(spacingClass);
          el.classList.add(spacingClass)
          // console.log("#" + el.id + " spaced " + spacingValue);
        })
      })
    }
    if (this.fullPageBackground) {
      this.fullPageBackground.forEach((background) => {
        parsed.querySelectorAll(background[0]).forEach((el) => {
          el.classList.add('moveToBackgroundImage')
        })
      })
    }
  }
}

Paged.registerHandlers(CSStoClass)

//float top

// lets you manualy add classes to some pages elements
// to simulate page floats.
// works only for elements that are not across two pages

let classElemFloatSameTop = 'page-float-same-top' // â† class of floated elements on same page
let classElemFloatSameBottom = 'page-float-same-bottom' // â† class of floated elements bottom on same page

let classElemFloatNextTop = 'page-float-next-top' // â† class of floated elements on next page
let classElemFloatNextBottom = 'page-float-next-bottom' // â† class of floated elements bottom on next page

class elemFloatTop extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller)
    this.experimentalFloatNextTop = []
    this.baseline = 22
    this.experimentalFloatNextBottom = []
    this.token
  }

  layoutNode(node) {
    // If you find a float page element, move it in the array,
    if (node.nodeType == 1 && node.classList.contains(classElemFloatNextTop)) {
      let clone = node.cloneNode(true)
      this.experimentalFloatNextTop.push(clone)
      // Remove the element from the flow by hiding it.
      node.style.display = 'none'
    }
    if (
      node.nodeType == 1 &&
      node.classList.contains(classElemFloatNextBottom)
    ) {
      let clone = node.cloneNode(true)
      this.experimentalFloatNextBottom.push(clone)
      // Remove the element from the flow by hiding it.
      node.style.display = 'none'
    }

    if (
      node.nodeType == 1 &&
      node.classList.contains(classElemFloatSameBottom)
    ) {
      let clone = node.cloneNode(true)
      // this.experimentalFloatNextBottom.push(clone);
      // Remove the element from the flow by hiding it.
      node.style.display = 'none'
    }
  }

  beforePageLayout(page, content, breakToken) {
    //console.log(breakToken);
    // If there is an element in the floatPageEls array,
    if (this.experimentalFloatNextTop.length >= 1) {
      // Put the first element on the page.
      page.element
        .querySelector('.pagedjs_page_content')
        .insertAdjacentElement('afterbegin', this.experimentalFloatNextTop[0])
      this.experimentalFloatNextTop.shift()
    }
    if (this.experimentalFloatNextBottom.length >= 1) {
      // Put the first element on the page.
      page.element
        .querySelector('.pagedjs_page_content')
        .insertAdjacentElement(
          'afterbegin',
          this.experimentalFloatNextBottom[0]
        )
      this.experimentalFloatNextBottom.shift()
    }
  }

  layoutNode(node) {
    if (node.nodeType == 1) {
      if (node.classList.contains(classElemFloatSameTop)) {
        let clone = node.cloneNode(true)
        clone.classList.add('figadded')
        document
          .querySelector('.pagedjs_pages')
          .lastElementChild.querySelector('article')
          .insertAdjacentElement('afterbegin', clone)
        node.style.display = 'none'
        node.classList.add('hide')
      }

      if (
        node.previousElementSibling?.classList.contains(classElemFloatSameTop)
      ) {
        let img = document
          .querySelector('.pagedjs_pages')
          .lastElementChild.querySelector(`.${classElemFloatSameTop}`)
        // console.log(img)
        // console.log(node)
        // if (img?.clientHeight) {
        //   // count the number of line for the image
        //   let imgHeightLine = Math.floor(img.clientHeight / this.baseline)
        //   // add one light and get the height in pixeol
        //   img.dataset.lineOffset = imgHeightLine + 0
        //   img.style.height = `${(imgHeightLine + 0) * this.baseline}px`
        // }
      }
    }
  }
  // works only with non breaked elements
  afterPageLayout(page, content, breakToken) {
    // try fixed bottom on same if requested
    if (page.querySelector('.' + classElemFloatSameBottom)) {
      var bloc = page.querySelector('.' + classElemFloatSameBottom)
      bloc.classList.add('absolute-bottom')
      bloc.classList.add('figadded')
    }

    // try fixed bottom if requested
    if (page.querySelector('.' + classElemFloatNextBottom)) {
      var bloc = page.querySelector('.' + classElemFloatNextBottom)
      bloc.classList.add('absolute-bottom')
      bloc.classList.add('figadded')
    }
  }
}
Paged.registerHandlers(elemFloatTop)

/* url cut*/

class urlcut extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller)
  }
  beforeParsed(content) {
    console.log('je coupe bien dans les url')
    //add wbr to / in links
    // transformed to add ZERO WIDTH SPACE (break opportunity) &#x200B;
    const links = content.querySelectorAll('a')
    links.forEach((link) => {
      if (!link.textContent.includes('http')) {
        return
      }
      // Rerun to avoid large spaces. Break after a colon or a double slash (//) or before a single slash (/), a tilde (~), a period, a comma, a hyphen, an underline (_), a question mark, a number sign, or a percent symbol.
      const content = link.textContent
      let printableUrl = content.replace(/\/\//g, '//\u200B')
      // put wbr around everything.
      //printableUrl = printableUrl.replace(/(\/|\~|\-|\=|\,|\_|\?|\#|\&|\;|\%)/g, "$1\u003Cwbr\u003E");
      printableUrl = printableUrl.replace(
        /(\/|\~|\-|\=|\,|\_|\?|\#|\&|\;|\%)/g,
        '$1\u003Cwbr\u003E'
      )
      printableUrl = printableUrl.replace(/\./g, '.\u200B')
      link.setAttribute('data-print-url', printableUrl)
      link.innerHTML = printableUrl
    })
  }
}
Paged.registerHandlers(urlcut)

class expMerge extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller)
    this.experimentalMerged = []
  }

  onDeclaration(declaration, dItem, dList, rule) {
    // alter the image
    //experimental merge
    if (declaration.property == '--experimental-merge') {
      let sel = csstree.generate(rule.ruleNode.prelude)
      sel = sel.replace('[data-id="', '#')
      sel = sel.replace('"]', '')
      let itemsList = sel.split(',')
      itemsList.forEach((elId) => {
        this.experimentalMerged.push([elId, declaration.value.value])
      })
    }
  }

  beforeParsed(parsed) {
    if (this.experimentalMerged.length > 0) {
      this.experimentalMerged.forEach((couple) => {
        const host = parsed.querySelector(couple[0])
        const guest = parsed.querySelector(couple[1])
        if (!host || !guest) {
          return
        }
        guest.style.display = 'none'
        host.classList.add('merged!')
        host.dataset.mergedGuest = guest.id
        host.insertAdjacentHTML('beforeend', guest.innerHTML)
      })
    }
  }
}

Paged.registerHandlers(expMerge)

class moveToParentFig extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller)
    this.moveToParentFig = []
  }
  onDeclaration(declaration, dItem, dList, rule) {
    // move the element to the next bit
    if (declaration.property == '--experimental-moveToOutsideFigure') {
      let sel = csstree.generate(rule.ruleNode.prelude)
      sel = sel.replace('[data-id="', '#')
      sel = sel.replace('"]', '')
      let itemsList = sel.split(',')
      itemsList.forEach((elId) => {
        this.moveToParentFig.push([elId, declaration.value.value])
      })
    }
  }
  beforeParsed(content) {
    if (this.moveToParentFig.length > 0) {
      this.moveToParentFig.forEach((elToPush) => {
        const elem = content.querySelector(elToPush[0])
        if (!elem) {
          return
        }
        let fighead = elem.querySelector('label').cloneNode(true)
        elem.insertAdjacentElement('beforeend', fighead)
        elem.closest('figure').insertAdjacentElement('afterend', elem)
      })
    }
  }
}
Paged.registerHandlers(moveToParentFig)
