const config = require('./config.js')

const { pdfHandler } = require('./modules/pdfhandler.js') //






// if there is a `node index.js file.zip`

const args = process.argv.slice(2)
if (args) {
  pdfHandler(args[0], 'outputs', `${args[0]}.pdf`)
} else {
  // otherwise
  pdfHandler(config.zipPath, 'outputs', 'testfile.pdf')
}

console.log(pdfHandler)
