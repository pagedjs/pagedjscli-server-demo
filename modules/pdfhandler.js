const fs = require("fs-extra");
const config = require("../config.js");
const get = require("lodash/get");
const path = require("path");
const axios = require("axios");
const FormData = require("form-data");
const crypto = require("crypto");
const { serviceHandshake } = require("./handshake.js");
const { writeLocallyFromReadStream } = require("./utils");

const pdfHandler = async (zipPath, outputPath, filename) => {
  // if (!accessToken) {
  const accessToken = await serviceHandshake();
  // }
  const serverUrl = `${config.protocol}://${config.host}${
    config.port ? `:${config.port}` : ""
  }`;

  const form = new FormData();
  form.append("zip", fs.createReadStream(zipPath));

  console.log("the server is:", serverUrl);
  return new Promise((resolve, reject) => {
    axios({
      method: "post",
      url: `${serverUrl}/api/htmlToPDF`,
      headers: {
        authorization: `Bearer ${accessToken}`,
        ...form.getHeaders(),
      },
      responseType: "stream",
      data: form,
    })
      .then(async (res) => {
        await writeLocallyFromReadStream(
          outputPath,
          filename,
          res.data,
          "binary"
        );
        resolve();
      })
      .catch(async (err) => {
        const { response } = err;
        console.log("there is an error here: ", err);
        if (!response) {
          return reject(new Error(`Request failed with message: ${err.code}`));
        }

        const { status, data } = response;
        const { msg } = data;

        // if (status === 401 && msg === 'expired token') {
        //   return pdfHandler(zipPath, outputPath, filename)
        // }

        return reject(
          new Error(`Request failed with status ${status} and message: ${msg}`)
        );
      });
  });
};

module.exports = { pdfHandler };
