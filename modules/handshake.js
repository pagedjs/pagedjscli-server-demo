const axios = require("axios");
const config = require("../config.js");
const serviceHandshake = async () => {
  const buff = Buffer.from(`${config.clientId}:${config.clientSecret}`, "utf8");
  const base64data = buff.toString("base64");

  console.log("hahaha1", base64data);
  console.log("hahaha2", config.clientId);
  console.log("hahaha3", config.clientSecret);

  const serviceURL = `${config.protocol}://${config.host}${
    config.port ? `:${config.port}` : ""
  }`;

  // check the health of the service
  const serviceHealthCheck = await axios({
    method: "get",
    url: `${serviceURL}/healthcheck`,
  });

  const { data: healthCheckData } = serviceHealthCheck;
  const { message } = healthCheckData;

  // if all is good no error
  if (message !== "Coolio") {
    throw new Error(`service ${which} is down`);
  }
  // then post the token to have an accessToken
  return new Promise((resolve, reject) => {
    axios({
      method: "post",
      url: `${serviceURL}/api/auth`,
      headers: { authorization: `Basic ${base64data}` },
    })
      .then(async ({ data }) => {
        const { accessToken } = data;
        resolve(accessToken);
      })
      .catch(async (err) => {
        const { response } = err;

        // if (foundServiceCredential) {
        //   await ServiceCredential.patchAndFetchById(foundServiceCredential.id, {
        //     accessToken: null,
        //   })
        // }
        console.log("AAAAAA", err);
        reject(err);

        // if (!response) {
        //   return reject(new Error(`Request failed with message: ${err.code}`))
        // }
        // const { status, data } = response
        // const { msg } = data
        // return reject(
        //   new Error(`Request failed with status ${status} and message: ${msg}`),
        // )
      });
  });
};

module.exports = {
  serviceHandshake,
};
