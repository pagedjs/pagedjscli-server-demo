const fs = require("fs-extra");
const writeLocallyFromReadStream = async (
  outputPath,
  filename,
  readerStream,
  encoding
) =>
  new Promise(async (resolve, reject) => {
    await fs.ensureDir(outputPath);
    const writerStream = fs.createWriteStream(
      `./${outputPath}/${filename}`,
      encoding
    );

    writerStream.on("close", () => {
      resolve();
    });
    writerStream.on("error", (err) => {
      reject(err);
    });
    readerStream.pipe(writerStream);
  });

module.exports = {
  writeLocallyFromReadStream,
};
