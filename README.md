# Pagedjs docker image example implementaion

## TLDR;

1. setting up the docker (`docker-compose up`)
2. update the config for `clientID` and `clientsecret` 
3. `npm install`
4. `node index.js` 

But seriously, read below, the config (like all config) can be a tad weird.

## 1. Setting up the docker.

There is a `docker-compose.yml` file ready to be used with `docker-compose up`. 

On first build, the docker will log a `clientID` and a `clientsecret`. Those
two needs to be added in the `config.js` file, as they’re use to create the
handshake between the server and the docker container.

The config file also needs to have the information about the protocol, the url
and the port of the docker container, with the path to the `.zip` file that will
be converted. 

The input needs to be a zip file containing at least a html file to render with
the content. 

It can also contains: 
- One single css file that will be parsed by paged.js (we can only have one file
  for now, so please avoid imports)
- One or multiple javascript hooks for paged.js, to allow dom manipulation
- The fonts (ttf and woff2 have been the most usable ones so far, but it should
  be working with other languages)

The images can be linked from another server (make sure you’re not
mixing https and http). 

## Run the app.

`npm install` to get the dependencies,
`node index.js` will run the script and send the zipfile to the docker
container, creating a pdf file in the output folder, ready to be looked at by
the designers. It’s using paged.js CLI in a linux environment, so you may have
differences if the layout was designed on an let’s say OSX.

